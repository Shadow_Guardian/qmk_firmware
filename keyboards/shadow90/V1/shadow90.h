/* Copyright 2018 Shadow_Guardian
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SHADOW90_H
#define SHADOW90_H

#include "quantum.h"

/* Keyboard layout
 * ,---------------------------------------------------------------------------.
 * |ESC| F1| F2| F3| F4| F5| F6| F7| F8| F9|F10|F11|F12|PRT|BRK|???|???|       | 17 keys
 * |---------------------------------------------------------------------------|
 * |  `|  1|  2|  3|  4|  5|  6|  7|  8|  9|  0|  -|  =|     BS|NML|???|???|???| 18 keys
 * |-----------------------------------------------------------------------|---|
 * |Tab  |  Q|  W|  E|  R|  T|  Y|  U|  I|  O|  P|  [|  ]|    \|  7|  8|  9|  +| 18 keys
 * |-----------------------------------------------------------------------|   |
 * |Caps  |  A|  S|  D|  F|  G|  H|  J|  k|  L|  ;|  '|     Ent|  4|  5|  6|   | 16 keys
 * |-----------------------------------------------------------------------|---|
 * |Shift   |  Z|  X|  C|  V|  B|  N|  M|  ,|  .|  /|     Shift|  1|  2|  3|ENT| 16 keys
 * |-----------------------------------------------------------------------|   |
 * |Ctrl| Win| Alt|          Space         | Alt| Win|Menu|Ctrl|    0  | . |   | 10 keys
 * `-----------------------------------------------------------------------`---'
 */

#define KEYMAP( \
    K000, K001, K002, K003, K004, K005, K006, K007, K008, K009, K010, K011, K012, K013, K014, K015, K016, \
    K100, K101, K102, K103, K104, K105, K106, K107, K108, K109, K110, K111,    K112,    K113, K114, K115, K116, K117, \
     K200,  K201, K202, K203, K204, K205, K206, K207, K208, K209, K210, K211,   K212,   K213, K214, K215, K216, K217, \
      K300,   K301, K302, K303, K304, K305, K306, K307, K308, K309, K310, K311,   K312,       K313, K314, K315, \
        K400,    K401, K402, K403, K404, K405, K406, K407, K408, K409, K410,        K411,     K412, K413, K414, K415, \
      K500,   K501,   K502,               K503,             K504,   K505,   K506,   K507,     K508,       K509 \
) { \
    {   K000,   K001,   K002,   K003,   K004,   K005,   K006,   K007,   K008,   K009,   K010,   K011,   K012,   K013,   K014,   K015,   KC_NO,  K016   }, \
    {   K100,   K101,   K102,   K103,   K104,   K105,   K106,   K107,   K108,   K109,   K110,   K111,   K112,   K113,   K114,   K115,   K116,   K117   }, \
    {   K200,   K201,   K202,   K203,   K204,   K205,   K206,   K207,   K208,   K209,   K210,   K211,   K212,   K213,   K214,   K215,   K216,   K217   }, \
    {   K300,   K301,   K302,   K303,   K304,   K305,   K306,   K307,   K308,   K309,   K310,   K311,   K312,   KC_NO,  K313,   K314,   K315,   KC_NO  }, \
    {   K400,   KC_NO,  K401,   K402,   K403,   K404,   K405,   K406,   K407,   K408,   K409,   K410,   K411,   KC_NO,  K412,   K413,   K414,   K415   }, \
    {   K500,   K501,   K502,   KC_NO,  KC_NO,  KC_NO,  K503,   KC_NO,  KC_NO,  K504,   K505,   K506,   KC_NO,  K507,   KC_NO,  K508,   K509,   KC_NO  } \
}

#endif
