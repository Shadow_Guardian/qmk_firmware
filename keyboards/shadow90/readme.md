# shadow90

![shadow90](https://imgur.com/a/G4YXjU3)

*A short description of the keyboard/project*

* Keyboard Maintainer: [Shadow Guardian](https://bitbucket.org/Shadow_Guardian)
* Hardware Supported: Handwired only  
* Hardware Availability: Custom availability only

Make example for this keyboard (after setting up your build environment):

    qmk compile -kb shadow90/v2 -km default


See the [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) and the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information. Brand new to QMK? Start with our [Complete Newbs Guide](https://docs.qmk.fm/#/newbs).

## Bootloader

Enter the bootloader in 2 ways:

* **Physical reset button**: Briefly press the button under the spacebar
* **Keycode in layout**: Press the key mapped to `RESET` if it is available (MO(1) + CAPS LOCK)
